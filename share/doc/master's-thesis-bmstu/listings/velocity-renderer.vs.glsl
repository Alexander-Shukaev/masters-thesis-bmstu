#version 400

uniform sampler3D _velocity_sampler;

uniform mat4 _mvp;

uniform uvec3 _sample_count;

uniform float _scale;

uniform vec3 _color = vec3(1, 1, 1);

in  vec3  _position;
in  float _shade;
out vec3  v_color;

vec4
RotationBetween(vec3 a, vec3 b) {
  float d = dot(a, b);
  float s = sqrt(dot(a, a) * dot(b, b));

  if (d / s == -1)
    return vec4(0, 0, 1, 0);

  return normalize(vec4(cross(a, b), d + s));
}

vec3
Rotate(vec4 q, vec3 v) {
  return v + 2 * cross(q.xyz, cross(q.xyz, v) + q.w * v);
}

void
main() {
  uint x =  gl_InstanceID %  _sample_count.x;
  uint y =  gl_InstanceID / (_sample_count.x * _sample_count.z);
  uint z = (gl_InstanceID % (_sample_count.x * _sample_count.z))
           /
           _sample_count.x;

  ivec3 translation = ivec3(x + 0.5, y + 0.5, z + 0.5);

  vec3 velocity = texelFetch(_velocity_sampler, translation, 0).rgb;
  vec4 rotation = RotationBetween(vec3(0, 0, 1), velocity);

  v_color = _shade * _color;

  gl_Position = _mvp * vec4(_scale * length(velocity)
                *
                Rotate(rotation, _position) + translation, 1);
}
