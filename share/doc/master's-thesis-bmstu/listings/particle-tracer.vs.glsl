#version 400

subroutine void    Pass();
subroutine uniform Pass _pass;

uniform sampler3D _velocity_sample;

uniform mat4 _mvp;

uniform uvec3 _texel_index_max;

uniform vec3 _position_max;

uniform float _time_step;
uniform float _amplifier;

in  vec3 _position;
out vec3 v_position;

subroutine (Pass)
void
Update() {
  vec3 _texel_index = clamp(_position - vec3(0.5, 0.5, 0.5),
                            vec3(0, 0, 0),
                            _position_max);
  vec3 velocity     = texture(_velocity_sample,
                              _texel_index / _texel_index_max).rgb;

  float time_step = _amplifier * _time_step;

  v_position = _position + time_step * velocity;

  _texel_index = clamp(v_position - vec3(0.5, 0.5, 0.5),
                       vec3(0, 0, 0),
                       _position_max);

  v_position = _position + 0.5 * time_step
               *
               (velocity + texture(_velocity_sample,
                                   _texel_index / _texel_index_max).rgb);

  v_position = clamp(v_position, vec3(0, 0, 0), _position_max);
}

subroutine (Pass)
void
Render() {
  gl_Position = _mvp * vec4(_position, 1);
}

void
main() {
  _pass();
}
