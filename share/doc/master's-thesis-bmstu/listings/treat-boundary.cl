__kernel
void
TreatBoundary(__global Configuration* configuration,
              __global char*          flag_field,
              __global double*        collision_field) {
  unsigned int id = get_global_id(0);

  if (id >= configuration->resolution)
    return;

  if (flag_field[id] == FLUID)
    return;

  __global double* boundary_cell = collision_field
                                   +
                                   id
                                   *
                                   lattice_velocity_count;
  int3             position;

  ComputePosition(id,
                  configuration->sizeX,
                  configuration->sizeY,
                  &position);

  for (int i = 0; i < lattice_velocity_count; ++i) {
    int3 new_position = position
                        +
                        convert_int3(configuration->lattice_velocities[i]);

    unsigned int index = CELL_INDEX(new_position,
                                    configuration->sizeX,
                                    configuration->sizeY);

    if (!IsFluidCellIndex(flag_field,
                          new_position,
                          index,
                          configuration->sizeX,
                          configuration->sizeY,
                          configuration->sizeZ))
      continue;

    __global double* fluid_cell = collision_field
                                  +
                                  index
                                  *
                                  lattice_velocity_count;

    switch (flag_field[id]) {
    case NO_SLIP:
      boundary_cell[i] = fluid_cell[lattice_velocity_count - 1 - i];

      break;

#ifdef MOVING_WALL_SCENARIO
    case MOVING_WALL: {
      double density;

      ComputeDensity(fluid_cell, &density);

      boundary_cell[i] = fluid_cell[lattice_velocity_count - 1 - i]
                         +
                         2.0
                         *
                         configuration->lattice_weights[i]
                         *
                         density
                         *
                         dot(configuration->lattice_velocities[i],
                             wall_velocity)
                         /
                         configuration->squared_lattice_speed_of_sound;

      break;
    }
#endif // MOVING_WALL_SCENARIO

    case OUTFLOW: {
      double  density;
      double3 velocity;
      double  equilibrium_density_fractions[lattice_velocity_count];

      ComputeDensity(fluid_cell, &density);
      ComputeVelocity(fluid_cell, density, &velocity, &configuration);
      ComputeEquilibriumDensityFractions(1.0,
                                         velocity,
                                         equilibrium_density_fractions,
                                         &configuration);
      boundary_cell[i] =
        equilibrium_density_fractions[lattice_velocity_count - 1 - i]
        +
        equilibrium_density_fractions[i]
        -
        fluid_cell[lattice_velocity_count - 1 - i];

      break;
    }

#ifdef INFLOW_SCENARIO
    case INFLOW: {
      double equilibrium_density_fractions[lattice_velocity_count];

      ComputeEquilibriumDensityFractions(1.0,
                                         inflow_velocity,
                                         equilibrium_density_fractions,
                                         &configuration);

      boundary_cell[i] = equilibrium_density_fractions[i];

      break;
    }
#endif // INFLOW_SCENARIO

    default:
      return;
    }
  }
}
