__kernel
void
UpdateMacroscopicQuantities(__global Configuration* configuration,
                            __global double*        collision_field,
                            __global float*         density_field,
                            __global float*         velocity_field) {
  int id = get_global_id(0);

  if (id >= configuration->resolution)
    return;

  if (flag_field[id] != FLUID)
    return;

  __global double* collision_cell = collision_field
                                    +
                                    id
                                    *
                                    lattice_velocity_count;

  double  density;
  double3 velocity;

  ComputeDensity(collision_cell, &density);
  ComputeVelocity(collision_cell, density, &velocity, &configuration);

  density_field[id] = density;

  velocity_field[id * 4 + 0] = velocity.x;
  velocity_field[id * 4 + 1] = velocity.y;
  velocity_field[id * 4 + 2] = velocity.z;
}
