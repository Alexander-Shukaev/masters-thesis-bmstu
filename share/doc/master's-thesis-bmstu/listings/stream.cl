__kernel
void
Stream(__global Configuration* configuration,
       __global char*          flag_field,
       __global double*        streaming_field,
       __global double*        collision_field) {
  unsigned int id = get_global_id(0);

  if (id >= configuration->resolution)
    return;

  if (flag_field[id] != FLUID)
    return;

  __global double* streaming_cell = streaming_field
                                    +
                                    id
                                    *
                                    lattice_velocity_count;

  int3 position;

  ComputePosition(id,
                  configuration->sizeX,
                  configuration->sizeY,
                  &position);

  for (int i = 0; i < lattice_velocity_count; ++i) {
    int3 new_position = position
                        -
                        convert_int3(configuration->lattice_velocities[i]);

    unsigned int index = CELL_INDEX(new_position,
                                    configuration->sizeX,
                                    configuration->sizeY);

    __global double* collision_cell = collision_field
                                      +
                                      index
                                      *
                                      lattice_velocity_count;

    streaming_cell[i] = collision_cell[i];
  }
}
