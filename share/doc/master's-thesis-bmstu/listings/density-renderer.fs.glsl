#version 400\n

uniform float _alpha = 0.1;

in  vec4 v_color;
out vec4 f_color;

void
main() {
  f_color = vec4(v_color.rgb, v_color.a * _alpha);
}
