__kernel
void
Collide(__global Configuration* configuration,
        __global char*          flag_field,
        __global double*        collision_field,
        __global float*         density_field,
        __global float*         velocity_field) {
  int id = get_global_id(0);

  if (id >= configuration->resolution)
    return;

  if (flag_field[id] != FLUID)
    return;

  __global double* collision_cell = collision_field
                                    +
                                    id
                                    *
                                    lattice_velocity_count;

  double  density = density_field[id];
  double3 velocity;

  velocity.x = velocity_field[id * 4 + 0];
  velocity.y = velocity_field[id * 4 + 1];
  velocity.z = velocity_field[id * 4 + 2];

  double equilibrium_density_fractions[lattice_velocity_count];

  ComputeEquilibriumDensityFractions(density,
                                     velocity,
                                     equilibrium_density_fractions,
                                     &configuration);

  ComputePostCollisionDensityFractions(collision_cell,
                                       configuration->tau,
                                       equilibrium_density_fractions);
}
