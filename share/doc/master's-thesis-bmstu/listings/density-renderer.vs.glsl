#version 400

uniform sampler3D _density_sampler;

subroutine vec3    PlanePosition();
subroutine uniform PlanePosition _plane_position;

uniform mat4 _mvp;

uniform vec3 _density_position_max;

uniform float _density_max;
uniform float _density_min;

uniform uvec3 _texel_index_max;

in  vec2 _position;
out vec4 v_color;

subroutine (PlanePosition)
vec3
PositionZY() {
  return vec3(gl_InstanceID, _position.y, _position.x);
}

subroutine (PlanePosition)
vec3
PositionXZ() {
  return vec3(_position.x, gl_InstanceID, _position.y);
}

subroutine (PlanePosition)
vec3
PositionXY() {
  return vec3(_position.x, _position.y, gl_InstanceID);
}

vec4
ToColor(float density) {
  float gamma = (density - _density_min) / (_density_max - _density_min);

  return vec4(-2 * gamma * gamma + 3 * gamma,
              -4 * gamma * gamma + 4 * gamma,
              -2 * gamma * gamma +     gamma + 1,
               4 * gamma * gamma - 4 * gamma + 1);
}

void
main() {
  vec3 position = _plane_position();

  vec3 density_position = clamp(position - vec3(0.5, 0.5, 0.5),
                                vec3(0, 0, 0),
                                _density_position_max);

  float density = texture(_density_sampler,
                          density_position / _texel_index_max).r;

  v_color = ToColor(density);

  gl_Position = _mvp * vec4(position, 1);
}
